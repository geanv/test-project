#include<stdio.h>

#define OUTMAIN(ops)					\
	void mp_hdlr_init_##ops(void);					\
	void __attribute__((constructor, used)) mp_hdlr_init_##ops(void)\
	{								\
		printf("Outside main function.\n");			\
	}

OUTMAIN();

int main()
{
    printf("Inside main function.\n");
}