
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <inttypes.h>

static inline uint64_t
rdtsc(void)
{
	union {
		uint64_t tsc_64;
		struct {
			uint32_t lo_32;
			uint32_t hi_32;
		};
	} tsc;

	asm volatile("rdtsc" :
		     "=a" (tsc.lo_32),
		     "=d" (tsc.hi_32));
	return tsc.tsc_64;
}

int main(int argc, char *argv[])
{
	struct timeval t1;
	uint64_t t2;
	struct timespec t3;
	int i;
	uint64_t max;

	if (argc == 1) {
		 max = atoi(argv[1]);
	}
	else {
		max = 100000000;
	}

	for (i = 0; i < max; i++) {
#ifdef RDTSC
		t2 = rdtsc();
#elif defined (CLOCK)
		clock_gettime(CLOCK_MONOTONIC_RAW, &t3);
#else
		gettimeofday(&t1, NULL);
#endif
	}
}